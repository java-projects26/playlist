package playlist;

import java.util.ArrayList;
import java.util.LinkedList;

public class Album {
	private String name;
	private String author;
	private ArrayList<Song> songs = new ArrayList<Song>();

	public Album(String name, String author) {
		super();
		this.name = name;
		this.author = author;

	}

	public boolean addSong(String title, double durations) {
		if (findSong(title) == null) {
			songs.add(new Song(title, durations));
			return true;
		}
		return false;
	}

	private Song findSong(String title) {
		for (Song songChecked : this.songs) {
			if (songChecked.getTitle().equals(title)) {
				return songChecked;
			}
		}
		return null;
	}

	public boolean addToPlaylist(int tracknumber, LinkedList<Song> playList) {
		int index = tracknumber - 1;
		if (index >= 0 && index < this.songs.size()) {
			playList.add(this.songs.get(index));
			return true;
		}
		System.out.println("The track number " + tracknumber +" is not found " );
		return false;

	}

	public boolean addToPlaylist(String title, LinkedList<Song> playList) {
		Song checkedSong = findSong(title);
		if (checkedSong != null) {
			playList.add(checkedSong);
			return true;
		}
		System.out.println("title " +title +" is not there in the album");
		return false;
	}

}