package playlist;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

public class Main {
	private static ArrayList<Album> albums = new ArrayList<Album>();
	private static Scanner scanner = new Scanner(System.in);
	private static LinkedList<Song> Playlist = new LinkedList<Song>();

	public static void main(String[] args) {

		Album album = new Album("Imagine Dragon", "Tim");
		album.addSong("Beliver", 4.5);
		album.addSong("Total", 4.6);
		album.addSong("Try your best", 3.5);
		album.addSong("Pain", 4.8);
		album.addSong("Trigger", 4.5);
		album.addSong("Future", 4.0);
		albums.add(album);

		album = new Album("ColdPlay", "Alien");
		album.addSong("Like", 4.5);
		album.addSong("Alone", 4.7);
		album.addSong("Till I die", 4.9);
		album.addSong("Fortune", 4.0);
		album.addSong("Twist", 4.9);
		album.addSong("Take", 5.0);
		albums.add(album);

		albums.get(0).addToPlaylist("Beliver", Playlist);
		albums.get(0).addToPlaylist("Trigger", Playlist);
		albums.get(1).addToPlaylist("Fortune", Playlist);
		albums.get(1).addToPlaylist("Twist", Playlist);
		albums.get(0).addToPlaylist("Total", Playlist);
		albums.get(0).addToPlaylist("Not there", Playlist);
		albums.get(0).addToPlaylist(3, Playlist);
		albums.get(0).addToPlaylist(25, Playlist);

		Play(Playlist);

	}

	private static void Play(LinkedList<Song> playlist) {
		ListIterator<Song> i = playlist.listIterator();
		if (playlist.size() == 0) {
			System.out.println("No songs in the playlist");

		} else {
			System.out.println("Now Plying " + i.next().toString());
		}
		printOption();

		boolean quit = false;
		boolean forward = true;
		while (!quit) {
			System.out.println("Enter your option");
			int option = scanner.nextInt();
			switch (option) {
			case 0:
				System.out.println("Exiting the playlist");
				quit = true;
				break;
			case 1:
				if (!forward) {
					if (i.hasNext()) {
						i.next();
					}
					forward = true;
				}
				if (i.hasNext()) {
					System.out.println("Now playing " + i.next().toString());
				} else {
					System.out.println("You are at the End of the list");
					forward = false;
				}
				break;
			case 2:
				if (forward) {
					if (i.hasPrevious()) {
						i.previous();
					}
					forward = false;
				}
				if (i.hasPrevious()) {
					System.out.println("Now playing " + i.previous().toString());
				} else {
					System.out.println("You are at the Start of the list");
					forward = true;
				}
				break;
			case 3:
				if (forward) {
					if (i.hasPrevious()) {
						System.out.println("Playing " + i.previous() + " again");
						forward = false;
					}
				} else {
					if (i.hasNext()) {
						System.out.println("Playing " + i.next() + " again");
						forward = true;
					}
				}
				break;
			case 4:
				printPlaylist();
				break;
			case 5:
				printOption();
				break;

			}
		}

	}

	private static void printPlaylist() {
		ListIterator<Song> i = Playlist.listIterator();
		if (Playlist.size() == 0) {
			System.out.println("No songs in the playlist");

		}
		while (i.hasNext()) {
			System.out.println(i.next().toString());
		}
	}

	private static void printOption() {
		System.out.println("Options \n"
				+ "\t 0 - Exit the playlist\n"
				+ "\t 1 - go to next song\n"
				+ "\t 2 - go to previous song\n"
				+ "\t 3 - replay the current song\n"
				+ "\t 4 - print the playlist\n"
				+ "\t 5 - print the options again\n");

	}
}
